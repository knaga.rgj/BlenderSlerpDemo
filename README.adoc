= Blender Slerp Demo
K.Naga <knaga.rgj@gmail.com>
v0.3, 2021/08/15


== 概要

目的: 姿勢Ａから姿勢Ｂへの回転を slerp で実現するときの、オイラー角 Roll-Pitch-Yaw の動きを可視化する。

注意: 再現性のため、モデル作成を含む操作の大部分を python script に書き下しています。

注意: blender のバージョンによっては動作しない可能性があります。 (v2.83, v2.93, Debian GNU/Linux で動作確認 2021/08/15, bullseye (testing) )

注意: blender の GUI 操作と API 操作がうまく整合できていないので、挙動がおかしくなることがあるかもしれません。

動画サンプルファイル: link:https://gitlab.com/knaga.rgj/BlenderSlerpDemo/-/wikis/Movies[wiki の Movies] 参照


=== 前提

* blender の基本操作ができる。（もしくは link:https://gitlab.com/knaga.rgj/BlenderSlerpDemo/-/wikis/QuickStart[wiki の QuickStart] でこと足りる。）
* python はわからなくもなく、基本はできる。
* 動画ファイル作成用に ffmpeg を入れてある（動画ファイル作らない場合は不要）。

=== 準備

1. SlerpDemo.py と PNG ファイル群を適当なディレクトリに置いてください。
2. SlerpDemo.py をエディタで開いて `TEXTURE_BASEDIR` `RENDERDIR` `CREATE_MOVIE_FILE_BY_FFMPEG` あたりを自分の環境にあわせて変更してください。
* `TEXTURE_BASEDIR` は「PNGファイル群を置いたディレクトリを**フルパス**で**末尾に `/` をつけて** ください。
* `RENDERDIR` は「画像シーケンスファイル群を保存するディレクトリ」を**フルパス**で指定してください。
3. blender を立ち上げ Scripting のワークスペースを開いてください。


== Quick Start

参考: link:https://gitlab.com/knaga.rgj/BlenderSlerpDemo/-/wikis/QuickStart[wiki の QuickStart]

以降は python コンソールで作業してください。なお SlerpDemo.py 等は /tmp/test ディレクトリに置いたものとしています。

1: python script のロード
----
fpath = '/tmp/test/SlerpDemo.py'
exec(compile(open(fpath).read(), fpath, 'exec'))
----

2: 各パス設定の確認（しなくてもよい）
----
import os
os.path.exists(STRIPE16_FILE_PATH)
os.path.isdir(RENDERDIR)
----

２つとも結果が 'True' になれば OK です


3: モデルの作成:
----
SlerpDemo.create_model()
----


4: 動作の生成: (ここでは [10,-20,30] から [-40, 50, 60] への回転とします)
----
slerp = SlerpDemo([10,-20,30], [-40, 50, 60])
----

オイラー角姿勢を `(roll(赤), pitch(緑), yaw(青))` の順に degree で扱っています。 +
リスト表記 `[]` だけでなくタプル表記 `()` でも構いません。


5: 鑑賞: blender の Layout ワークスペースでアニメーションを様々な視点で鑑賞してください。


6: 画像シーケンス保存 (CREATE_MOVIE_FILE_BY_FFMPEG が True なら動画ファイルも作成されます)

----
slerp.render_images("cam01", "pattern0-cam01")
----

少し時間がかかります。(１〜数分？） +
"cam01" は「カメラ名」で上方から見下ろすカメラ `cam00`〜`cam07`, 下方から見上げるカメラ `cam10`〜`cam17` のどれかを指定してください。 +
"pattern0-cam01" は画像シーケンスを保存するディレクトリ名で、`RENDERDIR` に作成され、そこにレンダリングされた画像シーケンスが保存されます。 +
CREATE_MOVIE_FILE_BY_FFMPEG が True の場合は `RENDERDIR` の下に pattern0-cam01.mp4 ファイルが作成されます。


== 応用
=== 姿勢指定

モデルが表示されている状態で実行してください。(`SlerpDemo.create_model()` を実行した後)

----
SlerpDemo.make_pose([10,-20,30])
----


=== 複数の動作パターンの切り替え

作成した動作オブジェクトの create_frames() を実行すると、blender のアニメーションが切り替わります。

----
slerp1 = SlerpDemo([10,-20,30], [-40, 50, 60])
slerp2 = SlerpDemo([-10,20,-30], [40, -50, -60])
slerp1.create_frames()
slerp2.create_frames()
----


=== 片道動作

create_frames() の引数に False を指定すると片道のみのアニメーションになります。 +
未指定時は True 指定として扱われ、往復動作が生成されます。

----
slerp = SlerpDemo([10,-20,30], [-40, 50, 60])
slerp.create_frames(False)
slerp.create_frames(True)
----


=== オイラーでの線形補間指定 (`elerp`)

slerp との比較に「オイラー角の lerp」での動作を生成できます（`elerp` を指定)。 +
未指定時は `slerp` が指定されたものとして扱われます。

----
sl = SlerpDemo([10,-20,30], [-40, 50, 60], 'slerp')
El = SlerpDemo([10,-20,30], [-40, 50, 60], 'elerp')
sl.create_frames()
El.create_frames()
----


=== 画麺サイズを指定して動画生成

----
slerp = SlerpDemo([10,-20,30], [-40, 50, 60])
slerp.render_images("cam00", "test0", (320, 180))
----


=== 全消去

----
SlerpDemo.clear_all()
----


=== モデルだけ表示

一旦全消去され、モデル（カメラ、証明含む）が配置されます。

----
SlerpDemo.create_model()
----

=== 回転軸関連

回転軸の方向

----
lp = SlerpDemo([10,-20,30], [-40, 50, 60])
lp.rot.axis
----

回転角

----
lp = SlerpDemo([10,-20,30], [-40, 50, 60])
lp.rot.angle
----



== 参考
=== SlerpDemo.demo0()

----
    @staticmethod
    def demo0():
        SlerpDemo.clear_all()
        SlerpDemo.create_model()
        sl=SlerpDemo([-20, -30, -40],[25, 35, 45])
        for cam in ['cam00', 'cam07', 'cam06']:
            dirname='demo0-'+cam
            sl.render_images(cam, dirname, (320, 180))
            pass
        return
----

=== SlerpDemo.demo1()

----
    @staticmethod
    def demo1():
        (wid,hei)=(640, 360)
        #(wid,hei)=(320, 180)
        stm=datetime.now()
        print(stm.strftime("%Y/%m/%d %H:%M:%S"))
        SlerpDemo.clear_all()
        SlerpDemo.create_model()
        for iptype in ['slerp', 'elerp']:
            sl=SlerpDemo([-60, 60, 0],[60, -60, 60], iptype)
            for cam in ['cam01', 'cam02']:  # , 'cam00', 'cam03', 'cam04']:
                dirname='demo1-%s-%s-%dx%d'%(iptype, cam, wid, hei)
                sl.render_images(cam, dirname, (wid, hei))
                pass
            pass
        etm=datetime.now()
        print(etm.strftime("%Y/%m/%d %H:%M:%S"))
        dt=(etm-stm).seconds
        ds=dt%60
        dt=int((dt-ds)/60)
        dm=dt%60
        dh=int((dt-dm)/60)
        print("%02d:%02d:%02d"%(dh,dm,ds))
        return
----


== 参考リンク集
* ＣＧ制作演習資料: http://web.wakayama-u.ac.jp/~tokoi/cgpe2020.html
* v2.93 マニュアル(日本語): https://docs.blender.org/manual/ja/2.93/
* v2.93 マニュアル(英語): https://docs.blender.org/manual/en/2.93/
* v2.93 API Scripting Hits: https://docs.blender.org/api/2.93/info_tips_and_tricks.html
* v2.93 API reference: https://docs.blender.org/api/2.93/
* 開発版 API reference: https://docs.blender.org/api/dev/
* mathutils reference: https://docs.blender.org/api/2.93/mathutils.html
** https://pypi.org/project/mathutils/
** https://gitlab.com/ideasman42/blender-mathutils
** 2021/08/15 時点で mathutils 自体のバージョンは 2.81.2

感謝: 各種ノウハウや Q&A を Web で公開してくれている方々と google 先生

== 解説(概略)

=== モデルについて

* X,Y,Z 軸はそれぞれ赤色, 緑色, 青色で表しています
* `Base` 座標系を接地させ、赤色の円柱（x軸向き）を割り当ててあります
* `Base` 座標系の先に `Roll` 座標系を配置し、赤色のフレームをとりつけてあります。`Roll` 座標系は X 軸まわりに回転させます。
* Roll フレームの表側には突起物がついていて表裏を目視で識別しやすくしてあります。
* `Roll` 座標系の先に `Pitch` 座標系を配置し、緑色のフレームをとりつけてあります。`Pitch` 座標系は Y軸まわりに回転させます。
* Pitch フレーム軸の両端と中央前方に突起物がついていて、ピッチ軸の回転角度が目視で識別しやすくしてあります。
* `Pitch` 座標系の先に `Yaw` 座標系を配置し、青色のフレームをとりつけてあります。`Yaw` 座標系は Z軸まわりに回転させます。
* 半透明の球を `Base` 座標系下に配置しています。２姿勢間の回転の回転軸のむきにあわせ「slerp では単一回転軸まわりの回転になる」ことを目視で確認しやすくしています。
* カメラは上方に８箇所(cam00〜cam07)、下方に８箇所(cam10〜cam17)配置してあります
* ライト（点光源）は各カメラの上方 0.5m のところに配置しています。ただし上方に配置した８個のカメラの上だけを対象とし、下方に配置したカメラにはライトを配置していません。

この Roll-Pitch-Yaw の配置は計算上は 'ZYX' のオイラー角表現になります。blender では 'XYZ' のオイラー角が標準なので操作やオプション指定に注意が必要です。

image:figs/rotdir_front.png[]
image:figs/rotdir_back.png[]


=== 重解・特異姿勢について

オイラー角表現では「２つの角度姿勢が同一の姿勢をとる」ことになります。 +
本モデルでは例えば (20, 30, 40) の姿勢は (20+180, 180-30, 40+180) の姿勢と同じになります。

image::figs/multi_root1.png[]
image::figs/multi_root2.png[]

本プログラムでは「姿勢クォータニオンからオイラー角の変換」において、ピッチ角が±90 の範囲内にある方を選択するようにしています。


また、本モデルでの特異姿勢は (any_r, -90, any_y)  と (any_r, 90, any_y) になり、any_r-any_y が同じ値であれば同一の姿勢をとることになります。 +
例えば (20, 90, 40) と (32, 90, 52) と (50, 90, 70) は全て同一の姿勢をとります。(any_r-any_y = 20)


=== 回転軸の計算について

blender の python コンソールでのクォータニオン演算は、mathutils モジュールによって行われています。

姿勢 q1 から 姿勢 q2 への回転は
----
rot=q1.rotation_diffrence(q2)
----
で得られるかと思ったのですが、
----
q1.rotate(rot)
----
の値が q2 とは異なるので、使用方法が違うか、バグか、よくわからない状況です。(2021/08/15 時点, link:https://gitlab.com/ideasman42/blender-mathutils/-/issues/25[Issue] 起こしてみました。)

本スクリプトでの実装では

姿勢 q1 から 姿勢 q2 への回転は下記のようにして求めています。
----
rot=q2 @ (q1.inverted())
if (rot.angle > math.pi):
    rot=q2 @ ((-q1).inverted())
    pass
----

