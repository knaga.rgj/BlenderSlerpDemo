#!/usr/bin/env python3
#
#  Copyright (c) 2021, knaga.rgj@gmail.com
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#  * Redistributions of source code must retain the above copyright notice,
#  this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the organization nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
#  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# rotation_difference() の挙動不審はバグかもしれず、とりあえず Issue きっておいてみた
# https://gitlab.com/ideasman42/blender-mathutils/-/issues/25

import mathutils
import math
from math import radians,sqrt
import os
import subprocess
from datetime import datetime


TEXTURE_BASEDIR='/tmp/test/'  # full path
STRIPE16_FILE_PATH=TEXTURE_BASEDIR+'stripe16.png'
CONECHECKER_FILE_PATH=TEXTURE_BASEDIR+'cone.png'

RENDERDIR='/tmp/test/rendered/'  # full path
VIDEO_WIDTH_DEFAULT=640
VIDEO_HEIGHT_DEFAULT=360
CREATE_MOVIE_FILE_BY_FFMPEG=True
#OVERLAY_PNGIMAGE_BASE=TEXTURE_BASEDIR+'cc-sa-screen-' # ex. cc-sa-screen-640x360.png
OVERLAY_PNGIMAGE_BASE=""

class SlerpDemo:
    def __init__(self, p1d, p2d, iptype='slerp'):
        self.p1r=list(map(radians, p1d))
        self.p2r=list(map(radians, p2d))
        iptypes=['slerp', 'elerp']
        if not iptype in iptypes:
            print("** unknown type: %s, msut be one of [%s]"%(iptype, ", ".join(iptypes)))
            iptype='slerp'
            pass
        self.iptype=iptype
        self.p1=mathutils.Euler(self.p1r, 'ZYX').to_quaternion()
        self.p2=mathutils.Euler(self.p2r, 'ZYX').to_quaternion()
        #self.rot=self.p1.rotation_difference(self.p2)  # なんか挙動不審
        self.rot=self.p2 @ (self.p1.inverted())
        if ((self.rot.angle > math.pi) or (self.rot.angle < -math.pi)):
            self.rot=self.p2 @ ((-self.p1).inverted())
            pass
        self.frames = int(self.rot.angle*60)+2
        uvz=mathutils.Vector([0,0,1])
        self.sphrot=uvz.rotation_difference(self.rot.axis).to_euler()
        self.create_frames()
        return
        
    def make_frame(self, frm):
        bpy.data.objects['SPH'].rotation_euler = self.sphrot
        if self.iptype=='slerp':
            p = self.p1.slerp(self.p2, frm/(self.frames-1)).to_euler('ZYX')
            SlerpDemo.noflip(p)
            bpy.data.objects['Roll'].rotation_euler.x = p.x
            bpy.data.objects['Pitch'].rotation_euler.y = p.y
            bpy.data.objects['Yaw'].rotation_euler.z = p.z
        elif self.iptype=='elerp':
            v1=mathutils.Vector(self.p1r)
            v2=mathutils.Vector(self.p2r)
            vi=v1.lerp(v2, frm/(self.frames-1))
            bpy.data.objects['Roll'].rotation_euler.x = vi.x
            bpy.data.objects['Pitch'].rotation_euler.y = vi.y
            bpy.data.objects['Yaw'].rotation_euler.z = vi.z
            pass
            
        return

    def create_frames(self, do_round_trip=True):
        names=['Roll', 'Pitch', 'Yaw']
        SlerpDemo.clear_frames(names)
        objs=list(map(lambda x: bpy.data.objects[x], names))
        for i in range(self.frames):
            bpy.context.scene.frame_set(i)
            self.make_frame(i)
            for obj in objs:
                bpy.context.view_layer.objects.active=obj
                obj.keyframe_insert(data_path='rotation_euler')
                pass
            pass
        if do_round_trip:  # 往復時 True, 片道時 False
            for i in range(self.frames):
                bpy.context.scene.frame_set(i+self.frames+1)
                self.make_frame(self.frames-i-1)
                for obj in objs:
                    bpy.context.view_layer.objects.active=obj
                    obj.keyframe_insert(data_path='rotation_euler')
                    pass
                pass

            bpy.context.scene.frame_start=0
            bpy.context.scene.frame_end=self.frames*2
        else:
            bpy.context.scene.frame_start=0
            bpy.context.scene.frame_end=self.frames
        return


    def render_images(self, cam, dirname, size=(VIDEO_WIDTH_DEFAULT, VIDEO_HEIGHT_DEFAULT)):
        self.create_frames()
        bpy.context.scene.render.resolution_x=size[0]
        bpy.context.scene.render.resolution_y=size[1]
        bpy.context.scene.render.filepath=RENDERDIR+os.sep+dirname+os.sep
        bpy.context.scene.camera=bpy.data.objects[cam]
        bpy.ops.render.render(animation=True)
        if CREATE_MOVIE_FILE_BY_FFMPEG:
            overlay_path=OVERLAY_PNGIMAGE_BASE+'%dx%d.png'%(size[0], size[1])
            if os.path.exists(overlay_path):
                cmd="cd %s; ffmpeg -r 30 -i %s/%%04d.png -i %s -filter_complex overlay -r 30 -pix_fmt yuv420p %s.mp4"
                cmd=cmd%(RENDERDIR, dirname, overlay_path, dirname)
            else:
                cmd="cd %s; ffmpeg -r 30 -i %s/%%04d.png -r 30  -pix_fmt yuv420p %s.mp4"%(RENDERDIR, dirname, dirname)
                pass
            #print(cmd)
            subprocess.run(cmd, shell=True)
            pass
        print("done.")
        return

    # 0 -> 0
    # -10 -> +10

    @staticmethod
    def noflip(rpy):
        if ((rpy[1] < -math.pi/2) or (rpy[1] > math.pi/2)):
            rpy[0]+=math.pi
            if (rpy[0]>math.pi): rpy[0]-=math.pi*2
            rpy[1]=math.pi-rpy[1]
            if (rpy[1]>math.pi): rpy[1]-=math.pi*2
            rpy[2]+=math.pi
            if (rpy[2]>math.pi): rpy[2]-=math.pi*2
            pass
        return
        
    @staticmethod
    def clear_frames(names=['Roll', 'Pitch', 'Yaw']):
        objs=list(map(lambda x: bpy.data.objects[x], names))
        for obj in objs:
            bpy.context.view_layer.objects.active=obj
            obj.animation_data_clear()
            pass

    @staticmethod
    def create_model(do_clear=True):
        if do_clear:
            SlerpDemo.clear_all()
            pass
        SlerpDemo.create_colors()
        SlerpDemo.create_base()
        SlerpDemo.create_roll()
        SlerpDemo.create_pitch()
        SlerpDemo.create_yaw()
        SlerpDemo.create_sphere()
        SlerpDemo.create_coordinate_arrows()
        SlerpDemo.create_cameras_and_lights()
        return

    @staticmethod
    def create_colors():
        bpy.data.materials.new(name='MyRed')
        red=bpy.data.materials['MyRed']
        red.use_nodes=False
        #red.roughness=1
        red.specular_intensity=0
        red.diffuse_color=(0.8, 0.08, 0.08, 1)
        
        bpy.data.materials.new(name='MyGreen')
        green=bpy.data.materials['MyGreen']
        green.use_nodes=False
        green.specular_intensity=0
        green.diffuse_color=(0.08, 0.8, 0.08, 1)
        
        bpy.data.materials.new(name='MyBlue')
        blue=bpy.data.materials['MyBlue']
        blue.use_nodes=False
        blue.specular_intensity=0
        blue.diffuse_color=(0.08, 0.25, 0.8, 1)

        # Pitch 軸にテクスチャ貼ろうとしてたときの名残
        #bpy.data.materials.new(name='MyTwoToneGreen')
        #lat=bpy.data.materials['MyTwoToneGreen']
        #lat.use_nodes=True
        #lat.shadow_method='NONE'
        #pBSDF = lat.node_tree.nodes["Principled BSDF"]
        #pBSDF.inputs['Roughness'].default_value=1.0
        #fullpath=TWOTONE_CYLINDER_FILE_PATH
        #img=lat.node_tree.nodes.new('ShaderNodeTexImage')
        #img.image=bpy.data.images.load(fullpath)
        #lat.node_tree.links.new(pBSDF.inputs['Base Color'], img.outputs['Color'])

        #回転軸キャップを白色半透明にしようとしてたときの名残
        #bpy.data.materials.new(name='MyWhite_skelton')
        #skl=bpy.data.materials['MyWhite_skelton']
        #skl.use_nodes=True
        #skl.blend_method='BLEND' # alpha blend
        #pBSDF = skl.node_tree.nodes["Principled BSDF"]
        #pBSDF.inputs['Base Color'].default_value=(1,1,1,1)
        #pBSDF.inputs['Alpha'].default_value=0.1

        bpy.data.materials.new(name='MyStripe_skelton')
        lat=bpy.data.materials['MyStripe_skelton']
        lat.use_nodes=True
        lat.blend_method='BLEND' # alpha blend
        lat.shadow_method='NONE'
        pBSDF = lat.node_tree.nodes["Principled BSDF"]
        pBSDF.inputs['Specular'].default_value=0
        pBSDF.inputs['Alpha'].default_value=0.2
        fullpath=STRIPE16_FILE_PATH
        img=lat.node_tree.nodes.new('ShaderNodeTexImage')
        img.image=bpy.data.images.load(fullpath)
        lat.node_tree.links.new(pBSDF.inputs['Base Color'], img.outputs['Color'])

        bpy.data.materials.new(name='MyConeChecker')
        lat=bpy.data.materials['MyConeChecker']
        lat.use_nodes=True
        lat.shadow_method='NONE'
        pBSDF = lat.node_tree.nodes["Principled BSDF"]
        fullpath=CONECHECKER_FILE_PATH
        img=lat.node_tree.nodes.new('ShaderNodeTexImage')
        img.image=bpy.data.images.load(fullpath)
        lat.node_tree.links.new(pBSDF.inputs['Base Color'], img.outputs['Color'])

        return

    @staticmethod
    def create_base():
        bpy.ops.object.add()
        node=bpy.data.objects['Empty']
        node.name = 'Base'
        cyl_rad=0.05
        cyl_len=0.1
        cyl_xoft=-0.4
        bpy.ops.mesh.primitive_cylinder_add(radius=cyl_rad, depth=cyl_len, rotation=(0, math.pi/2, 0), location=(cyl_xoft-cyl_len/2, 0, 0))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='mesh_Base'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])
        return

    @staticmethod
    def create_roll():
        bpy.ops.object.add()
        node=bpy.data.objects['Empty']
        node.name = 'Roll'
        node.parent=bpy.data.objects['Base']
        cyl_rad=0.4
        cyl_len=0.05
        hole_rad=0.35
        hole_len=cyl_len+0.02
        con_rad=0.02
        con_len=0.04
        # 先に hole を作る方が楽かも (operation 対象は active である必要がある？？？)
        bpy.ops.mesh.primitive_cylinder_add(vertices=128, radius=hole_rad, depth=hole_len, rotation=(0, 0, 0), location=(0, 0, 0))
        hole=bpy.data.objects['Cylinder']
        hole.name='mesh_Roll_Hole'
        bpy.ops.mesh.primitive_cylinder_add(vertices=128, radius=cyl_rad, depth=cyl_len, rotation=(0, 0, 0), location=(0, 0, 0))
        prim=bpy.data.objects['Cylinder']
        prim.name='mesh_Roll'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])
        bldiff=prim.modifiers.new(type='BOOLEAN', name='bldiff')
        bldiff.object=hole
        bldiff.operation='DIFFERENCE'
        bpy.ops.object.modifier_apply(modifier=bldiff.name)
        #bpy.ops.object.shade_smooth()
        bpy.data.objects.remove(hole)
        #
        place_rad=(cyl_rad+hole_rad)/2
        place_z=cyl_len/2+con_len/2
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, depth=con_len, rotation=(0, 0, 0), location=(place_rad, 0, place_z))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Roll_cone1'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, depth=con_len, rotation=(0, 0, 0), location=(-place_rad*0.5, place_rad*sqrt(3)/2, place_z))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Roll_cone2'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, depth=con_len, rotation=(0, 0, 0), location=(-place_rad*0.5, -place_rad*sqrt(3)/2, place_z))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Roll_cone3'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])
        return

    @staticmethod
    def create_pitch():
        bpy.ops.object.add()
        node=bpy.data.objects['Empty']
        node.name = 'Pitch'
        node.parent=bpy.data.objects['Roll']
        cyl_rad=0.02
        cyl_len=0.4*2+0.05
        brg_rad=cyl_rad*3
        brg_len=cyl_rad*2
        con_rad=0.02
        con_len=0.07
        bpy.ops.mesh.primitive_cylinder_add(vertices=128, radius=cyl_rad, depth=cyl_len, rotation=(math.pi/2, 0, 0), location=(0, 0, 0))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='mesh_Pitch'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        #prim.data.materials.append(bpy.data.materials['MyTwoToneGreen'])
        bpy.ops.mesh.primitive_cylinder_add(vertices=128, radius=brg_rad, depth=brg_len, rotation=(0, 0, 0), location=(0, 0, 0))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='mesh_Pitch_bearing'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, depth=con_len, rotation=(0, math.pi/2, 0), location=(brg_rad, 0, 0))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Pitch_cone1'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, depth=con_len, rotation=(0, math.pi/2, 0), location=(con_len/2, -cyl_len/2+con_rad, 0))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Pitch_cone2'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, depth=con_len, rotation=(0, math.pi/2, 0), location=(con_len/2, cyl_len/2-con_rad, 0))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Pitch_cone3'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        return

    @staticmethod
    def create_yaw():
        bpy.ops.object.add()
        node=bpy.data.objects['Empty']
        node.name = 'Yaw'
        node.parent=bpy.data.objects['Pitch']
        cyl_rad=0.2
        cyl_len=0.05
        con_rad=0.02
        con_len=0.3
        yaw_zoft=-0.045
        bpy.ops.mesh.primitive_cylinder_add(vertices=6, radius=cyl_rad, depth=cyl_len, rotation=(0, 0, math.pi/6), location=(0, 0, yaw_zoft))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='mesh_Yaw'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(0, 0, 0), location=(0, 0, con_len/2))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Yaw_c1'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(0, math.pi/2, 0), location=(con_len/2, 0, yaw_zoft))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Yaw_c2'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(math.pi/2, 0, 0), location=(0, -con_len/2, yaw_zoft))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Yaw_c3'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(-math.pi/2, 0, 0), location=(0, con_len/2, yaw_zoft))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_Yaw_c4'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])
        return

    @staticmethod
    def create_sphere():
        sph_rad=0.3
        bpy.ops.mesh.primitive_uv_sphere_add(radius=sph_rad)
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Sphere']
        prim.name='SPH'
        prim.parent=bpy.data.objects['Base']
        prim.data.materials.append(bpy.data.materials['MyStripe_skelton'])
        node=prim
        #cyl_rad=0.05
        #cyl_len=0.1
        #bpy.ops.mesh.primitive_cylinder_add(radius=cyl_rad, depth=cyl_len, rotation=(0, 0, 0), location=(0, 0, sph_rad+cyl_len/2))
        #bpy.ops.object.shade_smooth()
        #prim=bpy.data.objects['Cylinder']
        #prim.name='mesh_SPH_cyl'
        #prim.parent=node
        #prim.data.materials.append(bpy.data.materials['MyWhite_skelton'])
        con_rad=0.02
        con_len=0.1
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(0, 0, 0), location=(0, 0, sph_rad+con_len/2))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_SPH_cone_top'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyConeChecker'])
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(0, 0, 0), location=(0, 0, -sph_rad-con_len/2))
        #bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='mesh_SPH_cone_bottom'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyConeChecker'])
        return

    @staticmethod
    def create_coordinate_arrows():
        ### size parameters
        coord_arrows_position=(-0.5, 0.0, 0.1)
        cyl_len=0.1
        cyl_rad=0.01
        con_len=0.05
        con_rad=cyl_rad*2
        
        ### Origin
        bpy.ops.object.add()
        node=bpy.data.objects['Empty']
        node.name = 'Arrows'
        node.location=coord_arrows_position

        #### X-axis
        bpy.ops.mesh.primitive_cylinder_add(radius=cyl_rad, depth=cyl_len, rotation=(0, 1.5707963267948966, 0), location=(cyl_len/2, 0, 0))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='CylinderX'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])

        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(0, 1.5707963267948966, 0), location=(cyl_len+con_len/2, 0, 0))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='ConeX'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyRed'])

        #### Y-axis
        bpy.ops.mesh.primitive_cylinder_add(radius=cyl_rad, depth=cyl_len, rotation=(1.5707963267948966, 0, 0), location=(0, cyl_len/2, 0))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='CylinderY'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(-1.5707963267948966, 0, 0), location=(0, cyl_len+con_len/2, 0))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='ConeY'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyGreen'])
        
        #### Z-axis
        bpy.ops.mesh.primitive_cylinder_add(radius=cyl_rad, depth=cyl_len, rotation=(0, 0, 0), location=(0, 0, cyl_len/2))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cylinder']
        prim.name='CylinderZ'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])
        
        bpy.ops.mesh.primitive_cone_add(radius1=con_rad, radius2=0, depth=con_len, rotation=(0, 0, 0), location=(0, 0, cyl_len+con_len/2))
        bpy.ops.object.shade_smooth()
        prim=bpy.data.objects['Cone']
        prim.name='ConeZ'
        prim.parent=node
        prim.data.materials.append(bpy.data.materials['MyBlue'])

        return
    
    @staticmethod
    def create_cameras_and_lights():
        # カメラの位置と姿勢と番号、ライトはカメラの上方 +0.5 の位置
        geoms=[[[ 1.70, 0.00, 1], [60, 0,   90], '00'],
               [[ 1.20, 1.20, 1], [60, 0,  135], '01'],
               [[ 0.00, 1.70, 1], [60, 0,  180], '02'],
               [[-1.20, 1.20, 1], [60, 0, -135], '03'],
               [[-1.70, 0.00, 1], [60, 0,  -90], '04'],
               [[-1.20,-1.20, 1], [60, 0,  -45], '05'],
               [[ 0.00,-1.70, 1], [60, 0,    0], '06'],
               [[ 1.20,-1.20, 1], [60, 0,   45], '07'],
               [[ 1.70, 0.00, -1], [120, 0,   90], '10'],
               [[ 1.20, 1.20, -1], [120, 0,  135], '11'],
               [[ 0.00, 1.70, -1], [120, 0,  180], '12'],
               [[-1.20, 1.20, -1], [120, 0, -135], '13'],
               [[-1.70, 0.00, -1], [120, 0,  -90], '14'],
               [[-1.20,-1.20, -1], [120, 0,  -45], '15'],
               [[ 0.00,-1.70, -1], [120, 0,    0], '16'],
               [[ 1.20,-1.20, -1], [120, 0,   45], '17']]
        bpy.ops.object.add()
        cmpar=bpy.data.objects['Empty']
        cmpar.name = 'cams'
        bpy.ops.object.add()
        ltpar=bpy.data.objects['Empty']
        ltpar.name = 'lits'
        for (loc,rot,idx) in geoms:
            rot = list(map(radians, rot))
            bpy.ops.object.camera_add(location=loc, rotation=rot)
            cm = bpy.data.objects['Camera']
            cm.name='cam'+idx
            cm.parent=cmpar
            if (loc[2]>0):  # Z正側だけライト配置
                loc[2]+=0.5
                bpy.ops.object.light_add(type='POINT', location=loc)
                lt = bpy.data.objects['Point']
                lt.name='lit'+idx
                lt.parent=ltpar
                lt.data.energy=100
                pass
            pass
              
        
        return
    
    @staticmethod
    def clear_all():
        for mat in bpy.data.materials:
            bpy.data.materials.remove(mat)
            pass
        for obj in bpy.context.scene.objects:
            bpy.data.objects.remove(obj)
            pass
        return

    @staticmethod
    def make_pose(pose):
        bpy.data.objects['Roll'].rotation_euler = mathutils.Vector((1,0,0))*radians(pose[0])
        bpy.data.objects['Pitch'].rotation_euler = mathutils.Vector((0,1,0))*radians(pose[1])
        bpy.data.objects['Yaw'].rotation_euler = mathutils.Vector((0,0,1))*radians(pose[2])
        return

    @staticmethod
    def demo0():
        SlerpDemo.clear_all()
        SlerpDemo.create_model()
        sl=SlerpDemo([-20, -30, -40],[25, 35, 45])
        for cam in ['cam00', 'cam07', 'cam06']:
            dirname='demo0-'+cam
            sl.render_images(cam, dirname, (320, 180))
            pass
        return
    
    @staticmethod
    def demo1():
        (wid,hei)=(640, 360)
        #(wid,hei)=(320, 180)
        stm=datetime.now()
        print(stm.strftime("%Y/%m/%d %H:%M:%S"))
        SlerpDemo.clear_all()
        SlerpDemo.create_model()
        for iptype in ['slerp', 'elerp']:
            sl=SlerpDemo([-60, 60, 0],[60, -60, 60], iptype)
            for cam in ['cam01', 'cam02']:  # , 'cam03', 'cam05', 'cam07', 'cam11', 'cam13', 'cam15', 'cam17']:
                dirname='demo1-%s-%s-%dx%d'%(iptype, cam, wid, hei)
                sl.render_images(cam, dirname, (wid, hei))
                pass
            pass
        etm=datetime.now()
        print(etm.strftime("%Y/%m/%d %H:%M:%S"))
        dt=(etm-stm).seconds
        ds=dt%60
        dt=int((dt-ds)/60)
        dm=dt%60
        dh=int((dt-dm)/60)
        print("%02d:%02d:%02d"%(dh,dm,ds))
        return
    
    pass
